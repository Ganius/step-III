export default class Component {
    constructor(props) {
        this.props = props;
    }

    /*
     *  attributes: [[attributeName, attributeValue],[],[]]
     */

    createElem(tagName= "div", text= "", attributes = []) {
        const elem = document.createElement(tagName);
        elem.innerHTML = text;

        if(attributes.length) {
            attributes.forEach(([attribute, value]) => {
                elem.setAttribute(attribute, value)
            });
        }
        return elem;
    }

    addElement(child, parent= document.body) {
        parent.append(child);
    }

    addButton(thisButtonName, place, text, listener, typeListener, onceValue, attributes = []) {
        this[thisButtonName] = this.createElem('button', text);
        this.addElement(this[thisButtonName], place);
        this[thisButtonName].addEventListener(typeListener, listener, {once: onceValue});
        if (attributes.length) {
            attributes.forEach(([attribute, value]) => {
                this[thisButtonName].setAttribute(attribute, value)
            });
        }
    }


}