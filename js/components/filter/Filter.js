import Component from '../Component.js';
import Input from '../form/Input.js';
import Select from '../form/Select.js';
import {VisitCardiologist, VisitDentist, VisitTherapist} from "../visitCard";

export default class Filter extends Component {

    constructor(props) {
        super();
        this.props = props;
    }

    render() {
        this.div = document.createElement('div');
        this.div.classList.add('filter');
        document.querySelector('.container').prepend(this.div);
        this.form = document.createElement('form');
        this.div.prepend(this.form);
        this.form.classList.add('filterForm')

        this.input = new Input ('filter__input', 'Title', this.form);
        this.selectUrgency = new Select('filter__selectUrgency', 'Urgency', 'urgency', this.form);
        this.selectUrgency.addOption('Please choose an urgency level');
        this.selectUrgency.addOption('low');
        this.selectUrgency.addOption('normal');
        this.selectUrgency.addOption('high');

        this.selectStatus = new Select('filter__selectStatus', 'Status', 'status', this.form);
        this.selectStatus.addOption('Please choose a status');
        this.selectStatus.addOption('opened');
        this.selectStatus.addOption('closed');

        this.button = document.createElement('button');
        this.button.innerText = 'Filter';
        this.button.classList.add('filter__button');
        document.querySelector('.filter').append(this.button);
        this.button.addEventListener('click', this.filter.bind(this));
    }

    filter() {
        this.input = document.querySelector('.filter__input');
        let val = this.input.value;
        this.selectUrgency = document.querySelector('.filter__selectUrgency');
        this.selectStatus = document.querySelector('.filter__selectStatus');
        let filtered = this.props;

        let cards = document.querySelectorAll('.visit-card');
        cards.forEach(e => e.classList.add('hider'))

        if (this.selectUrgency.value === 'Please choose an urgency level' &&  this.selectStatus.value === 'Please choose a status'
            && this.input.value === ''
        ) {
            filtered = this.props;
        }
        if (this.selectStatus.value === 'opened') {
            filtered = filtered.filter(card => card.status === 'opened');
        }
        if ( this.selectStatus.value === 'closed') {
            filtered = filtered.filter(card => card.status === 'closed');
        }
        if ( this.selectUrgency.value === 'high') {
            filtered = filtered.filter(card => card.urgency === 'high');
        }
        if (this.selectUrgency.value === 'low' ) {
            filtered = filtered.filter(card => card.urgency === 'low');
        }
        if (this.selectUrgency.value === 'normal') {
            filtered = filtered.filter(card => card.urgency === 'normal');
        }
        if (this.input.value !== '') {
            filtered = filtered.filter(function(card){
                for (let key in card ) {
                    if (card[key] === val)
                        return card
                }
            })
        }
        filtered.forEach(el => {
            if (el.doctor === 'Cardiologist') {
                new VisitCardiologist(el)
            }
            if (el.doctor === "Dentist") {
                new VisitDentist(el)
            }
            if (el.doctor === "Therapist") {
                new VisitTherapist(el)
            }
        })
    }
}



