import Component from '../Component.js';
import {putDataOnServe, postCarsOnServe} from "../../functions";

export default class Form extends Component{
    constructor(props, cardElem, showMore) {
        super();
        this.counter = 0;
        this.card = cardElem;
        this.addElement(this.formContainer);

        if (props === null) {
            this.newVisitCreating = true;
            this.addDoctorSelect();
        } else {
            this.props = props;
            this.createLinesHtml(this.props);
            this.addLines(this.html);
        }

        this.addCloseButton();
    }

    doctorSelect = `
                <form class="form">
                <select class="form__doctor-selection" name="doctor">
                        <option value="" selected disabled>Выберите врача</option>
                        <option value="Cardiologist">Cardiologist</option> 
                        <option value="Dentist">Dentist</option>
                        <option value="Therapist">Therapist</option>
                    </select>   
                </form>`;

    additionalCardiologistHtml = `
                <div class="additionalDoctorHtml">
                    Цель визита: <input name="title" required>
                    Комментарий: <input name="description">
                    Срочность:
                    <select class="form__doctor-selection" name="urgency">
                        <option value="high">Высокая</option> 
                        <option value="normal" selected>Нормальная</option>
                        <option value="low">Низкая</option> 
                    </select>
                    Давление: <input name="pressure">
                    Индекс массы тела<input name="bmi" >
                    Были сердечно-сосудистые заболевания?
                    <select class="form__doctor-selection" name="wasHardIll">
                        <option value="true">Да</option> 
                        <option value="false" selected>Нет</option>
                    </select>
                    Возраст: <input name="age"">
                    ФИО<input name="userName" required>
                    <button class="submit" type="button" >Сохранить</button>
                </div>`;

    cardiologistHtml = ({
                            bmi = '', description = '', pressure = '', status = '', title = '', urgency = '',
                            userName = '', wasHardIll = 'false', age = ''
                        }) => {

        let urgencyValueHigh, urgencyValueNormal, urgencyValueLow, illTrue, illFalse, statusOpened, statusClosed;

        urgency === 'high' ? urgencyValueHigh = 'selected' :
            urgency === 'normal' ? urgencyValueNormal = 'selected' :
                urgency === 'low' ? urgencyValueLow = 'selected' : null;


        wasHardIll === 'true' ? illTrue = 'selected' : illFalse = 'selected';
        status === 'opened' ? statusOpened = 'selected' : statusClosed = 'selected';

        return `
                 <form class="form">
                    Врач:
                    <select class="form__doctor-selection" name="doctor">
                        <option value="Cardiologist">Cardiologist</option> 
                        <option value="Dentist" selected>Dentist</option>
                        <option value="Therapist" >Therapist</option>
                    </select> 
                    Статус: 
                    <select class="form__doctor-selection" name="status">
                        <option value="opened" ${statusOpened}>Открыт</option> 
                        <option value="closed" ${statusClosed}>Закрыт</option>
                    </select>
                    Срочность:
                    <select class="form__doctor-selection" name="urgency">
                        <option value="high" ${urgencyValueHigh}>Высокая</option> 
                        <option value="normal" ${urgencyValueNormal}>Нормальная</option>
                        <option value="low" ${urgencyValueLow}>Низкая</option> 
                    </select>
                    ФИО<input name="userName" value="${userName}" required>
                    Были сердечно-сосудистые заболевания?
                    <select class="form__doctor-selection" name="wasHardIll">
                        <option value="true" ${illTrue}>Да</option> 
                        <option value="false" ${illFalse}>Нет</option>
                    </select>
                    Возраст: <input name="age" value="${age}">
                    Комментарий: <input name="description" value="${description}">
                    Давление: <input name="pressure" value="${pressure}">
                    Цель визита: <input name="title" value="${title}" required>
                    Индекс массы тела<input name="bmi" value="${bmi}">
                    <button class="submit" type="button" >Сохранить</button>
                </form>`
    };


    additionalDentistHtml = `
                <div class="additionalDoctorHtml">
                    Цель визита: <input name="title" required>
                    Комментарий: <input name="description">
                    Срочность:
                    <select class="form__doctor-selection" name="urgency">
                        <option value="high">Высокая</option> 
                        <option value="normal" selected>Нормальная</option>
                        <option value="low">Низкая</option> 
                    </select>
                    Дата последнего посещения: <input name="date">
                    Возраст: <input name="age"">
                    ФИО<input name="userName" required>
                    <button class="submit" type="button" >Сохранить</button>
                </div>`;

    dentistHtml = ({date = '', description = '', status = '', title = '', urgency = '', userName = '', age = ''}) => {

        let urgencyValueHigh, urgencyValueNormal, urgencyValueLow, statusOpened, statusClosed;

        urgency === 'high' ? urgencyValueHigh = 'selected' :
            urgency === 'normal' ? urgencyValueNormal = 'selected' :
                urgency === 'low' ? urgencyValueLow = 'selected' : null;

        status === 'opened' ? statusOpened = 'selected' : statusClosed = 'selected';

        return `
                 <form class="form">
                    <select class="form__doctor-selection" name="doctor">
                        <option value="Cardiologist">Cardiologist</option> 
                        <option value="Dentist" selected>Dentist</option>
                        <option value="Therapist" >Therapist</option>
                    </select>
                    Статус: 
                    <select class="form__doctor-selection" name="status">
                        <option value="opened" ${statusOpened}>Открыт</option> 
                        <option value="closed" ${statusClosed}>Закрыт</option>
                    </select>
                    Срочность:
                    <select class="form__doctor-selection" name="urgency">
                        <option value="high" ${urgencyValueHigh}>Высокая</option> 
                        <option value="normal" ${urgencyValueNormal}>Нормальная</option>
                        <option value="low" ${urgencyValueLow}>Низкая</option> 
                    </select>
                    ФИО<input name="userName" value="${userName}" required>
                    Возраст: <input name="age" value="${age}">
                    Комментарий: <input name="description" value="${description}">
                    Цель визита: <input name="title" value="${title}" required>
                    Дата последнего посещения: <input name="date" value="${date}">
                    <button class="submit" type="button" >Сохранить</button>
                </form>`
    };

    additionalTherapistHtml = `
                 <div class="additionalDoctorHtml">
                    Цель визита: <input name="title" required>
                    Комментарий: <input name="description">
                    Срочность:
                        <select class="form__doctor-selection" name="urgency">
                        <option value="high">Высокая</option> 
                        <option value="normal" selected>Нормальная</option>
                        <option value="low">Низкая</option> 
                    </select>
                    Возраст: <input name="age"">
                    ФИО<input name="userName" required>
                    <button class="submit" type="button" >Сохранить</button>
                 </div>`;

    therapistHtml = ({description = '', status = '', title = '', urgency = '', userName = '', age = ''}) => {
        let urgencyValueHigh, urgencyValueNormal, urgencyValueLow, statusOpened, statusClosed;
        urgency === 'high' ? urgencyValueHigh = 'selected' :
            urgency === 'normal' ? urgencyValueNormal = 'selected' :
                urgency === 'low' ? urgencyValueLow = 'selected' : null;

        status === 'opened' ? statusOpened = 'selected' : statusClosed = 'selected';

        return `
                <form class="form">
                    <select class="form__doctor-selection" name="doctor">
                        <option value="Cardiologist">Cardiologist</option> 
                        <option value="Dentist">Dentist</option>
                        <option value="Therapist" selected>Therapist</option>
                    </select> 
                    Статус: 
                    <select class="form__doctor-selection" name="status">
                        <option value="opened" ${statusOpened}>Открыт</option> 
                        <option value="closed" ${statusClosed}>Закрыт</option>
                    </select>
                    Срочность:
                    <select class="form__doctor-selection" name="urgency">
                        <option value="high" ${urgencyValueHigh}>Высокая</option> 
                        <option value="normal" ${urgencyValueNormal}>Нормальная</option>
                        <option value="low" ${urgencyValueLow}>Низкая</option> 
                    </select>
                    ФИО<input name="userName" value="${userName}">
                    Возраст: <input name="age" value="${age}">
                    Комментарий: <input name="description" value="${description}">
                    Цель визита: <input name="title" value="${title}" required>
                    <button class="submit" type="button" >Сохранить</button>
                </form>`;
    };

    formContainer = this.createElem('div', null, [['class', 'form-container']]);

    addDoctorSelect() {

        this.formContainer.insertAdjacentHTML('afterbegin', this.doctorSelect);
        const doctorSelection = document.querySelector('.form__doctor-selection');
        doctorSelection.addEventListener('change', ({target}) => {

            //hide previous additional HTML if user have changed value of doctor
            const arr = Array.from(document.querySelectorAll('.additionalDoctorHtml'));
            arr.length > 0 ? arr[0].remove() : null;

            target.value === 'Therapist' ? this.addLines(this.additionalTherapistHtml) :
                target.value === 'Cardiologist' ? this.addLines(this.additionalCardiologistHtml) :
                    target.value === 'Dentist' ? this.addLines(this.additionalDentistHtml) : false;
        })
    }


    createLinesHtml(props) {
        const {doctor} = props;

        doctor === 'Therapist' ? this.html = this.therapistHtml(props) :
            doctor === 'Cardiologist' ? this.html = this.cardiologistHtml(props) :
                doctor === 'Dentist' ? this.html = this.dentistHtml(props) : new Error('Empty doctor value!!!');
    }

    addLines(html) {
        if (this.formContainer.querySelector('.form__doctor-selection')) {
            const container = document.querySelector('.form');
            container.insertAdjacentHTML('beforeend', html);
        } else {
            this.formContainer.insertAdjacentHTML('beforeend', html);
        }

        this.form = this.formContainer.querySelector('.form');

        this.editButton = this.formContainer.querySelector('[class="submit"]');
        this.editButton.addEventListener('click', (e) => {

            const formData = new FormData(this.form);

            const linesWithUserChanges = {};
            for (let [name, value] of formData) {
                if (name === 'doctor') {

                    if (value !== 'Cardiologist' && value !== 'Dentist' && value !== 'Therapist') {
                        alert('You need to fill line "doctor" correct!!!');
                        throw new Error('You need to fill all lines!!!');
                    }

                }
                linesWithUserChanges[name] = value;
            }

            if (this.newVisitCreating) {
                postCarsOnServe(linesWithUserChanges)
                    .then(() => location.reload());
            } else {
                putDataOnServe(linesWithUserChanges, this.props.id)
                    .then(() => location.reload());
            }

        });

        this.listener(this.formContainer, this.counter);
    }

    listener(formContainer, counter) {

        if (this.formContainer) {
            document.body.addEventListener('click', (e) => {

                if (counter > 0 && !this.formContainer.contains(e.target)) {
                    this.formContainer.remove();
                    counter = 0;
                    location.reload();
                } else {
                    ++counter;
                }
            });
        }
    };

    addCloseButton() {
        this.addButton('closeButton', this.formContainer, 'X', this.closeButtonListener, 'click', true, [['class', 'button-close']]);
    }

    closeButtonListener = () => {
        this.formContainer.remove();
    }
}
