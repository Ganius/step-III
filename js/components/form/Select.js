import Component from '../Component.js';
export default class Select extends Component {
    constructor (className, labelText, selectName, parent) {
        super();
        this.selectAdd(className, labelText, selectName, parent);
    }
    selectAdd (className, labelText, selectName, parent) {
        this.select = this.createElem('select', null, [['class', className],
            ['name', selectName]]);
        this.addElement(this.select, parent);
        this.label = this.createElem('label', null, [['htmlFor', labelText]]);
        this.label.innerHTML = labelText;
        this.select.before(this.label);
    }
    addOption (optionText) {
        this.option = document.createElement('option');
        this.option.innerText = optionText;
        this.select.append(this.option);
    }
}

