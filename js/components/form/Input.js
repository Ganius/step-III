import Component from '../Component.js';
export default class Input extends Component{
    constructor(className, labelText, parent, inputName) {
        super();
        this.inputAdd(className, labelText, parent, inputName);
    }

    inputAdd (className, labelText, parent, inputName ='title') {
        this.input = this.createElem('input', null, [['class', className],
            ['name', inputName]]);
        this.addElement(this.input, parent);
        this.label = this.createElem('label', null, [['htmlFor', 'title']]);
        this.label.innerHTML = labelText;
        this.input.before(this.label);
    }
}



