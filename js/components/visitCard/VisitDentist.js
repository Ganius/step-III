import Visit from './Visit.js';

export default class VisitDentist extends Visit{
    constructor(props) {
        super(props);
        this.additionalInformation(props)
    }

    additionalInformation(props) {
        const additionalInfoHtml = this.formDentistInfo(props);
        this.addVisit(additionalInfoHtml);
    }

    formDentistInfo(props) {

        const arr = [];
        for (let i in props) {
            if (props[i] !== '') {
                arr.push(this.makeVisitLineInput(i, props[i]));
            }
        }

        this.visitShowMoreHtml = arr.join('');
    }

}