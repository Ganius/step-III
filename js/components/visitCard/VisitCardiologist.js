import Visit from './Visit.js';

export default class VisitCardiologist extends Visit {
    constructor(props) {
        super(props);
        this.additionalInformation(props)
    }

    additionalInformation(props) {
        const additionalInfoHtml = this.formCardiologistInfo(props);
        this.addVisit(additionalInfoHtml);
    }

    formCardiologistInfo(props) {

        const arr = [];
        for (let i in props) {
            if (props[i] !== '') {
                arr.push(this.makeVisitLineInput(i, props[i]));
            }
        }

        this.visitShowMoreHtml = arr.join('');
    }

}


