import Component from '../Component.js';
import {Form} from '../form/index.js';
import {deleteCardFromServe} from "../../functions/index.js";

export default class Visit extends Component{
    constructor(props) {
        super();
        this.props = props;
        const {userName = '', title = '', doctor, description = '', id, status = '', urgency = '', ...rest} = props;
        this.title = title;
        this.doctor = doctor;
        this.description = description;
        this.cardId = id;
        this.userName = userName;
        this.urgency = urgency;
        this.status = status;
        this.container.id = this.cardId;
        this.dragAndDrop();
    }

    visitShowMoreHtml;
    container = this.createElem('div', null, [['class', 'visit-card']]);


    dragAndDrop() {
        this.container.setAttribute('draggable', true);
        this.cardsField = document.querySelector('.cards-container');
        this.cards = document.querySelector('.visit-card');
        this.container.addEventListener('drop', () => {
            this.nodes = Array.prototype.slice.call(this.cardsField.children);
            this.draggable = document.querySelector('.dragging');
            if (this.nodes.indexOf(this.container) > this.nodes.indexOf(this.draggable)) {
                this.container.after(this.draggable);
            }
            if (this.nodes.indexOf(this.container) < this.nodes.indexOf(this.draggable) ) {
                this.container.before(this.draggable);
            }
        });
        this.container.addEventListener('dragover', function (e) {
            e.preventDefault();
        });
        this.container.addEventListener('dragstart', () => {
            this.container.classList.add('dragging')
        });
        this.container.addEventListener('dragend', () => {
            this.container.classList.remove('dragging')
        })
    }
    showMoreInformationListener = () => {
        this.container.insertAdjacentHTML('beforeend', this.visitShowMoreHtml);
        this.showMore.remove();
    };

    editButtonListener = () => {
        this.editCard.remove();
        new Form(this.props, null, this.visitShowMoreHtml);
    };

    deleteCardListener = () => {
        const answer =  confirm('Удаляем???');
        if (answer) {
            deleteCardFromServe(this.cardId)
                .then(() => location.reload());
        }
    };

    addVisit(additionalInfoHtml) {
        this.additionalInfoHtml = additionalInfoHtml;

        const doctor = this.createElem('p', `<span class="visit-line__title">Врач: </span><br>${this.doctor}`,
            [['class', 'visit-line__container']]);
        const status = this.createElem('p', `<span class="visit-line__title">Статус визита: </span><br>${this.status}`,
            [['class', 'visit-line__container']]);
        const urgency = this.createElem('p', `<span class="visit-line__title">Срочность: </span><br>${this.urgency}`,
            [['class', 'visit-line__container']]);
        const userName = this.createElem('p', `<span class="visit-line__title">ФИО: </span><br>${this.userName}`,
            [['class', 'visit-line__container']]);

        this.addButton('editCard', this.container, 'Edit card', this.editButtonListener, 'click', false, [['class', 'button-visit']]);
        this.addButton('deleteCard', this.container, 'Delete card', this.deleteCardListener, 'click', false, [['class', 'button-visit']]);

        [status, doctor, urgency, userName].forEach(elem => this.addElement(elem, this.container));

        this.addElement(this.container, document.querySelector('.cards-container'));

        this.addButton('showMore', this.container, 'Show more information', this.showMoreInformationListener, 'click', true);

    }

    makeVisitLineInput(name, value) {

        if (name === 'wasHardIll') {

            let ill = value === 'true' ? 'Были проблемы' : 'Жалоб нет';

            return `<p class="visit-line__container"><span class="visit-line__title">Были сердечно-сосудистые заболевания: </span><br>${ill}</p>`
        }

        return name === 'age' ?
            `<p class="visit-line__container"><span class="visit-line__title">Возраст: </span><br>${value}</p>` :
            name === 'description' ?
                `<p class="visit-line__container"><span class="visit-line__title">Комментарий: </span><br>${value}</p>` :
                name === 'pressure' ?
                    `<p class="visit-line__container"><span class="visit-line__title">Обычное давление: </span><br>${value}</p>` :
                    name === 'bmi' ?
                        `<p class="visit-line__container"><span class="visit-line__title">Индекс массы тела: </span><br>${value}</p>` :
                        name === 'weight' ?
                            `<p class="visit-line__container"><span class="visit-line__title">Вес: </span><br>${value}</p>` :
                            name === 'date' ?
                                `<p class="visit-line__container"><span class="visit-line__title">Дата последнего визита: </span><br>${value}</p>` :
                                name === 'title' ?
                                    `<p class="visit-line__container"><span class="visit-line__title">Цель визита: </span><br>${value}</p>` : null;
    }
}