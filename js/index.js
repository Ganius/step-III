import Landing from './classLanding.js';
import Authorization from "./classAuth.js";

import {Visit, Component, VisitTherapist, VisitDentist, VisitCardiologist} from './components/index.js';
import getDataFromServe from './functions/getDataFromServe.js';
import {Form} from "./components/form/index.js";

import FilterTest from './components/filter/Filter.js';
import Filter from './components/filter/Filter.js';

const data = getDataFromServe();
const page = new Landing();

const container = document.querySelector('.container');
const cardsContainer = document.createElement('div');
cardsContainer.classList.add('cards-container');
container.append(cardsContainer);

const auth = new Authorization();


data.then((cards) => {

    let filter2 = new Filter(cards);
    filter2.render();

    cards.forEach(card => {
        if (card.doctor === 'Cardiologist') {
            return new VisitCardiologist(card)
        } else if (card.doctor === "Dentist") {
            return new VisitDentist(card)
        } else if (card.doctor === "Therapist") {
            return new VisitTherapist(card)
        } else {
            throw new Error('Something goes wrong! Come back in few minutes');
        }
    });
})
    .then(() => {

        const createNewCardButton = document.createElement('button');
        createNewCardButton.innerText = 'Create new card';
        createNewCardButton.classList.add('filter__button');
        const container = document.querySelector('.filter');
        container.append(createNewCardButton);
        createNewCardButton.addEventListener('click', (e) => {
            e.preventDefault();
            new Form(null);
        });

    });
