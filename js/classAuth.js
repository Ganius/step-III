import setCookie from "./functions/setCookie.js";
import getCookie from "./functions/getCookie.js";

export default class Authorization {
    constructor() {
        this.getAuthForm();
        this.checkForCookies();
    }

    getAuthForm() {
        const form = document.querySelector('.auth-form');
        form.addEventListener('submit', this.authWithEmailAndPassword);
    }

    authWithEmailAndPassword(event) {
        event.preventDefault();

        const container = document.querySelector('.container');
        const modal = document.querySelector('.modal');
        const loginButton = document.querySelector('.login-btn');
        const div = document.querySelector('.no-items-div');

        const email = event.target.querySelector('#email').value;
        const password = event.target.querySelector('#password').value;

        const data = fetch('http://cards.danit.com.ua/login', {
            method: 'POST',
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'Success') {
                    setCookie('userToken', data.token, {'max-age': 60000});
                    alert('Authorization successful!');
                    modal.style.display = 'none';
                    loginButton.style.display = 'none';
                    container.style.display = 'block';
                    div.style.display = 'none';
                } else if (data.status === 'Error') {
                    alert('Wrong email or password. Please try again!');
                }
            })
    }

    checkForCookies() {
        const container = document.querySelector('.container');
        const modal = document.querySelector('.modal');
        const loginButton = document.querySelector('.login-btn');
        const div = document.querySelector('.no-items-div');

        if (getCookie('userToken')) {
            modal.style.display = 'none';
            loginButton.style.display = 'none';
            container.style.display = 'block';
            div.style.display = 'none';
        }
    }
}