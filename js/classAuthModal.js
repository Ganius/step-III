export default class AuthModal {
    constructor() {
        this.createModalWindow();
        this.createDismissButton();
        this.createLoginForm();
        this.checkForError();
    }

    header = document.querySelector('.header');

    createModalWindow() {
        const modal = document.createElement('div');
        const loginButton = document.createElement('button');

        loginButton.innerText = 'Log in';
        loginButton.classList.add('btn');
        loginButton.classList.add('login-btn');
        loginButton.addEventListener('click', this._buttonListener);
        modal.classList.add('modal');

        document.body.append(modal);
        this.header.append(loginButton);
    }

    _buttonListener() {
        const modal = document.querySelector('.modal');
        modal.style.display = 'flex';
    }

    createDismissButton() {
        const modalWindow = document.querySelector('.modal');
        const close = document.createElement('span');

        close.classList.add('close');
        close.addEventListener('click', () => {
            modalWindow.style.display = 'none';
        });

        modalWindow.append(close);
    }

    createLoginForm() {
        const modalWindow = document.querySelector('.modal');
        const authForm = document.createElement('form');
        authForm.classList.add('auth-form');
        authForm.innerHTML =
            `
            <h1 class="form-header">Login</h1>
            <form>
                <div>
                    <input type="email" id="email" placeholder="Email" required>
                </div>
                <div>
                    <input type="password" id="password" placeholder="Password" required>
                </div>
                <button type="submit" class="auth-btn">Log in</button>
            </form>`;

        modalWindow.append(authForm);
    }

    checkForError() {
        const modalWindow = document.querySelector('.modal');
        const emailInput = document.getElementById('email');
        const errorDiv = document.createElement('div');
        errorDiv.setAttribute('id', 'error');

        emailInput.onblur = function () {
            if (!emailInput.value.includes('@') || !emailInput.value.includes('.')) {
                emailInput.classList.add('invalid');
                errorDiv.innerHTML = 'Пожалуйста, введите правильный email.'
                modalWindow.append(errorDiv);
            }
        };

        emailInput.onfocus = function () {
            if (this.classList.contains('invalid')) {
                this.classList.remove('invalid');
                errorDiv.innerHTML = "";
            }
        }
    }
}