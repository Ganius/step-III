export default async function postDataOnServe(data) {

    return await fetch(`http://cards.danit.com.ua/cards`, {
        method: 'POST',
        body: JSON.stringify({
            ...data
        }),
        headers: {
            Authorization: "Bearer 395b8015ac30",
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
        .then(response => response.json()).
        then(data => console.log(data));

}