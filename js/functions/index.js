export {default as getDataFromServe} from './getDataFromServe.js';
export {default as putDataOnServe} from './putDataOnServe.js';
export {default as postCarsOnServe} from './postCardOnServe.js';
export {default as deleteCardFromServe} from './deleteCardFromServe.js';
export {default as getCookie} from './getCookie.js';
export {default as setCookie} from './setCookie.js';