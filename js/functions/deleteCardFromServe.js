export default async function deleteCardFromServe(cardId) {

    return await fetch(`http://cards.danit.com.ua/cards/${cardId}`, {
        method: 'DELETE',
        headers: {
            Authorization: "Bearer 395b8015ac30"
        }
    })
        .then(response => {

            response.json()
        })
        .then(data => console.log(data));

}