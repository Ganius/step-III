export default async function putDataOnServe(data, cardsId) {

    return await fetch(`http://cards.danit.com.ua/cards/${cardsId}`, {
        method: 'PUT',
        body: JSON.stringify({
            ...data
        }),
        headers: {
            Authorization: "Bearer 395b8015ac30",
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
    .then(response => response.json()).
    then(data => console.log(data));

}