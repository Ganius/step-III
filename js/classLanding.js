import AuthModal from "./classAuthModal.js";

export default class Landing {
    constructor() {
        this.createHeader();
        this.authModal = new AuthModal();
        this.createInfoDiv();
    }


    createHeader() {
        const header = document.createElement('div');

        header.classList.add('header');
        header.innerHTML = `<img class="logo" src="img/logo.png" alt="logo">`;

        document.body.prepend(header);
    }

    createInfoDiv() {
        const div = document.createElement('div');

        div.classList.add('no-items-div');
        div.innerText = 'no items have been added';

        document.body.append(div);
    }
}